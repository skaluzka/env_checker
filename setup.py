#!/bin/env python2

# at least python 2.7.x is required
import sys
if not sys.version_info[:2] >= (2, 7):
    print('error! at least python 2.7.x is required')
    print('found:')
    print(sys.version)
    sys.exit(1)


import os
import shutil
from setuptools import setup


def fetch_version():
    here = os.path.realpath(os.path.dirname(__file__))
    sys.path.append(os.path.join(here, 'env_checker'))
    from env_checker.version import __version__ as ver
    if not ver:
        print('ver =', ver)
        print("error! couldn't fetch version :(")
        sys.exit(1)
    else:
        return ver


#copying README file to env_checker/ dir so it will be also a part of sdist
shutil.copy('README', 'env_checker')


#running setup from distutils.core
setup(name='env_checker',
    version=fetch_version(),
    description='check your environment before do anything',
    long_description=open('README').read(),
    author='Sebastian Kaluzka',
    author_email='skaluzka@riseup.net',
    url='https://bitbucket.org/skaluzka/env_checker',
    download_url='https://bitbucket.org/skaluzka/env_checker/branch/master',
    packages=['env_checker',],
    package_data={
        'env_checker': [
            'check_env.sh',
            'env_checker.xml',
            'prod_env_example.xml',
            'README'
        ],
    },
    scripts=['env_checker/start_check_env.sh'],
    zip_safe=False,
)


#clean up in env_checker/ dir
os.remove(os.path.join('env_checker', 'README'))




#70D0:

