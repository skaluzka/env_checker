#!/bin/bash
#set -v
#set -e
#set -x


time (
DIST_DIR=dist
PKG_NAME=$(python2 setup.py --fullname)
echo "PKG_NAME=$PKG_NAME"

#removing package dir (if exists) inside dist/ dir
if [ -d "$DIST_DIR/$PKG_NAME" ]; then
    echo "removing package dir \"$DIST_DIR/$PKG_NAME\" please wait..."
    rm -rf $DIST_DIR/$PKG_NAME
    rm -f MAINFEST
    rm -rf env_checker.egg-info
    echo "package dir \"$DIST_DIR/$PKG_NAME\" removed"
else
    echo "skipping -> couldn't find package dir: \"$DIST_DIR/$PKG_NAME\""
fi

#building source distribution (sdist) package
python2 setup.py -v sdist --formats=gztar,zip

#unpacking
if [ $? -eq 0 ]; then
    cd $DIST_DIR
    unzip $PKG_NAME.zip
    cd -
    echo "done"
else
    echo "error"
fi
)




#70D0:
# - test *.zip package before extraction
# - add python detecting

