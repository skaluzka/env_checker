#!/bin/bash
#set -v
#set -e
#set -x

time python2 -m doctest test_env_checker.tst -v
if [ $? -gt 0 ]; then
    tput setaf 1
    echo "some test(s) failed"
    tput sgr0
    exit 1
else
    tput setaf 2
    echo "all tests passed! :)"
    tput sgr0
fi




#70D0:

