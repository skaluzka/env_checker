>>> import sys

# adding upper level directory to module paths
>>> sys.path.append('..')

# importing EnvChecker class from env_checker module
>>> from env_checker import EnvChecker

#######################################################################
#
# object creation tests
#
#######################################################################

>>> ech1 = EnvChecker(xml_file_path='test01.xml') # doctest: +ELLIPSIS
>>> ech2 = EnvChecker(xml_file_path='test01.xml') # doctest: +ELLIPSIS
>>> ech1 == ech2
False

>>> ech3 = EnvChecker(xml_file_path='test01.xml') # doctest: +ELLIPSIS
>>> ech2 == ech3
False

>>> ech = EnvChecker(xml_file_path='test01.xml', debug_level='DEBUG') # doctest: +ELLIPSIS
20...- env_checker.__init__ - DEBUG - self = <env_checker.EnvChecker object at ...>
20...- env_checker.__load_xml_file - DEBUG - loading input file: "test01.xml" please wait...
20...- env_checker.__load_xml_file - DEBUG - loading input file finished

>>> ech = EnvChecker(xml_file_path='test02.xml', debug_level='DEBUG') # doctest: +ELLIPSIS
20...- env_checker.__init__ - DEBUG - self = <env_checker.EnvChecker object at ...>
20...- env_checker.__load_xml_file - DEBUG - loading input file: "test02.xml" please wait...
20...- env_checker.__load_xml_file - DEBUG - loading input file finished

>>> ech = EnvChecker(xml_file_path='test02.xml', debug_level='DEBUG') # doctest: +ELLIPSIS
20...- env_checker.__init__ - DEBUG - self = <env_checker.EnvChecker object at ...>
20...- env_checker.__load_xml_file - DEBUG - loading input file: "test02.xml" please wait...
20...- env_checker.__load_xml_file - DEBUG - loading input file finished

>>> ech = EnvChecker() # doctest: +ELLIPSIS
Traceback (most recent call last):
...
    ech = EnvChecker() # doctest: +ELLIPSIS
TypeError: __init__() takes at least 2 arguments (1 given)

>>> ech = EnvChecker(debug_level='INFO') # doctest: +ELLIPSIS
Traceback (most recent call last):
...
    ech = EnvChecker(debug_level='INFO') # doctest: +ELLIPSIS
TypeError: __init__() takes at least 2 arguments (2 given)

>>> ech = EnvChecker(debug_level='ABC') # doctest: +ELLIPSIS
Traceback (most recent call last):
...
    ech = EnvChecker(debug_level='ABC') # doctest: +ELLIPSIS
TypeError: __init__() takes at least 2 arguments (2 given)

#######################################################################
#
# testing logger only
#
#######################################################################

>>> ech = EnvChecker(xml_file_path='test01.xml') # doctest: +ELLIPSIS

>>> ech = EnvChecker(xml_file_path='test01.xml', debug_level='INFO') # doctest: +ELLIPSIS

>>> ech = EnvChecker(xml_file_path='test01.xml', debug_level='WARNING') # doctest: +ELLIPSIS

>>> ech = EnvChecker(xml_file_path='test01.xml', debug_level='NOTSET') # doctest: +ELLIPSIS

>>> ech = EnvChecker(xml_file_path='test01.xml', debug_level='DEBUG') # doctest: +ELLIPSIS
20...- env_checker.__init__ - DEBUG - self = <env_checker.EnvChecker object at ...>
20...- env_checker.__load_xml_file - DEBUG - loading input file: "test01.xml" please wait...
20...- env_checker.__load_xml_file - DEBUG - loading input file finished

#######################################################################
#
# loading valid input xml files
#
#######################################################################

>>> ech = EnvChecker(xml_file_path='test01.xml', debug_level='DEBUG') # doctest: +ELLIPSIS
20...- env_checker.__init__ - DEBUG - self = <env_checker.EnvChecker object at ...>
20...- env_checker.__load_xml_file - DEBUG - loading input file: "test01.xml" please wait...
20...- env_checker.__load_xml_file - DEBUG - loading input file finished

#######################################################################
#
# parsing valid input xml file
#
#######################################################################

>>> ech.parse_xml_file_content() # doctest: +ELLIPSIS
20...- env_checker.parse_xml_file_content - DEBUG - parsing content of test01.xml file -> required entries, please wait...
20...- env_checker.parse_xml_file_content - DEBUG - parsing content of test01.xml file -> optional entries, please wait...

#######################################################################
#
# loading invalid input xml file
#
#######################################################################

>>> ech = EnvChecker(xml_file_path='test02.xml', debug_level='DEBUG') # doctest: +ELLIPSIS
20...- env_checker.__init__ - DEBUG - self = <env_checker.EnvChecker object at ...>
20...- env_checker.__load_xml_file - DEBUG - loading input file: "test02.xml" please wait...
20...- env_checker.__load_xml_file - DEBUG - loading input file finished

#######################################################################
#
# parsing invalid input xml file
#
#######################################################################

>>> ech.parse_xml_file_content() # doctest: +ELLIPSIS
Traceback (most recent call last):
...
    sys.exit(self.EXIT_CODE_ERROR)
SystemExit: 1




#70D0:

