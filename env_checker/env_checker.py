#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
package name: env_checker
module name : env_checker.py
description : This module contains one class called EnvChecker which
can be used for checking production environment before start any real
tasks or job. Script is highly customizable via configuration *.xml
file which contains two major sections: <required> and <optional>.
With regexps you can specify the exact versions of tools being checked.
There is also nice mechanism for determining which entries are more
important than others. Skipping procedures are also implemented. Please
see env_checker.xml file for some examples and study cli help output
for some details
"""


# Importing python 2.7.x standard modules
import sys
if not sys.version_info[:2] >= (2, 7):
    print('error! at least python 2.7.x is required')
    print('found:')
    print(sys.version)
    sys.exit(1)

import argparse
import logging
import os
import re
import subprocess
import xml.etree.ElementTree as et

# Importing version from external module
from version import __version__

__prog__ = 'env_checker.py'


# Create logger
logger = logging.getLogger(__name__)
# Create console handler and set level to none
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.NOTSET)
# Create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s.%(funcName)s - %(levelname)s - %(message)s')
# Add formatter to ch
ch.setFormatter(formatter)
# Add ch to logger
logger.addHandler(ch)




class EnvChecker(object):
    """
    description:
        The only one class inside env_checker.py module. Class contains
        a few methods: __init__ (initialization method), method for
        loading input *.xml file: __load_xml_file, method for parsing
        content of that file:  parse_xml_file_content and finally the
        most important method: check_env for checking programs in current
        environment (launching programs and parsing version output)
    """


    RET_CODE_ERROR = 1
    RET_CODE_SUCCESS = 0

    log_levels = {'WARNING': logging.WARNING,
                  'INFO': logging.INFO,
                  'DEBUG': logging.DEBUG,
                  'NOTSET': logging.NOTSET}


    def __init__(self, xml_file_path,
                 exit_required=1, exit_req_ver=1, skip_required=0,
                 exit_optional=0, exit_opt_ver=0, skip_optional=0,
                 debug_level='INFO'):
        """
        description:
            This is an init method of EnvChecker class
        input args:
            - xml_file_path - full path to *.xml configuration file
            - exit_required - 0/1 flag used for managing required
              entries. If equal 1 then script will be stopped for first
              not found required entry. Otherwise only warning will be
              displayed
            - exit_req_ver - 0/1 flag used for managing required
              entries. If equal 1 then script will be stopped for first
              required entry with wrong version. Otherwise only warning
              will be displayed
            - skip_required - boolean flag for skipping parsing for
              required entries
            - exit_optional - 0/1 flag used for managing optional
              entries. If equal 1 then script will be stopped for first
              not found optional entry. Otherwise only warning will be
              displayed
            - exit_opt_ver - 0/1 flag used for managing optional
              entries. If equal 1 then script will be stopped for first
              optional entry with wrong version. Otherwise only warning
              will be displayed
            - skip_optional - boolean flag for skipping parsing for
              optional entries
            - debug_level - for handling debug level by logger object
              created outside EnvChecker class
        return value:
            - none
        """
        self.xml_file_path = xml_file_path
        # Variables used for handling required entries
        self.exit_required = exit_required
        self.exit_req_ver = exit_req_ver
        self.skip_required = skip_required
        # Variables used for handling optional entries
        self.exit_optional = exit_optional
        self.exit_opt_ver = exit_opt_ver
        self.skip_optional = skip_optional
        # Variables initialized as None
        self.required_entries = None
        self.optional_entries = None
        # Fetching and setting debug level
        self.debug_level = debug_level
        logger.setLevel(self.log_levels[self.debug_level])
        # Display self in debug mode
        logger.debug('self = {}'.format(self))
        # Loading xml file content
        self.xml_file_content = self.__load_xml_file()


    def __load_xml_file(self):
        """
        description:
            This method loads xml file (path is stored in variable
            self.xml_file_path) and return its content as python
            string
        input args:
            - none
        return value:
            - function returns python string - content of input *.xml
              file
        """
        tmp = None
        logger.debug('loading input file: "{}" please wait...'.format(self.xml_file_path))
        with open(self.xml_file_path, 'r') as file_handler:
            tmp = file_handler.read()
        logger.debug('loading input file finished')
        return tmp


    def parse_xml_file_content(self):
        """
        description:
            This method parses content of xml file (path is stored in
            variable self.xml_file_path) and creates two lists called:
            self.required_entries and self.optional_entries
            respectively for all entries into <required> tags and for
            all entries into <optional> tags. please see docstring for
            _load_xml_file method
        input args:
            - none
        return value:
            - none
        """
        try:
            tree_root = et.ElementTree(et.fromstring(self.xml_file_content))
        except et.ParseError as ex:
            logger.error('{} file parse error: {}'.format(self.xml_file_path, ex.message))
            sys.exit(self.RET_CODE_ERROR)
        if not self.skip_required:
            logger.debug('parsing content of {} file -> required entries, please wait...'.format(self.xml_file_path))
            self.required_entries = [((r.find('prog_dir').text if r.find('prog_dir').text else ''),
                                      r.find('prog_name').text,
                                      r.find('ver_switch').text,
                                      r.find('ver_regexp').text)
                                     for r in tree_root.findall('required/entry')]
        else:
            logger.info('skipping parsing for required entries: self.skip_required={}'.format(self.skip_required))
            self.required_entries = []
        if not self.skip_optional:
            logger.debug('parsing content of {} file -> optional entries, please wait...'.format(self.xml_file_path))
            self.optional_entries = [((o.find('prog_dir').text if o.find('prog_dir').text else ''),
                                      o.find('prog_name').text,
                                      o.find('ver_switch').text,
                                      o.find('ver_regexp').text)
                                     for o in tree_root.findall('optional/entry')]
        else:
            logger.info('skipping parsing for optional entries: self.skip_optional={}'.format(self.skip_optional))
            self.optional_entries = []


    def check_env(self):
        """
        description:
            This method checks current environment according to input
            data parsed from configuration xml file and adequately to
            setting provided from cli
        input args:
            - none
        return value:
            - method returns self.RET_CODE_SUCCESS (0) if both lists
              self.required_entries and self.optional_entries are empty
              or if none error was faced. Otherwise function will
              return self.RET_CODE_ERROR (1)
        """
        if not self.required_entries + self.optional_entries:
            logger.debug('self.required_entries = {}'.format(self.required_entries))
            logger.debug('self.optional_entries = {}'.format(self.optional_entries))
            return self.RET_CODE_SUCCESS
        for xml_entry_list in (('required', self.required_entries), ('optional', self.optional_entries)):
            entry_category = xml_entry_list[0]
            logger.debug('entry_category = {}'.format(entry_category))
            for entry in xml_entry_list[-1]:
                prog = entry[1]
                logger.info('checking {} please wait...'.format(prog))
                logger.debug('entry = {}'.format(entry))
                cmd_list = (os.path.join(entry[0], entry[1]), entry[2])
                logger.debug('cmd_list = {}'.format(cmd_list))
                ver_regexp_str = entry[-1]
                ver_regexp = re.compile(ver_regexp_str)
                try:
                    proc = subprocess.Popen(cmd_list,
                                            stdout=subprocess.PIPE,
                                            stderr=subprocess.PIPE,
                                            shell=False)
                    std_out, std_err = proc.communicate()
                except OSError:
                    tmp_dict = {'required': self.exit_required, 'optional': self.exit_optional}
                    if tmp_dict[entry_category]:
                        logger.error('error! an error occurred for command: {}'.format(cmd_list))
                        return self.RET_CODE_ERROR
                    else:
                        logger.warning('warning! an error occurred for command: {}'.format(cmd_list))
                        continue
                text_lines = (std_out + std_err).splitlines()
                regexp_matched = 0
                for i in xrange(len(text_lines)):
                    logger.debug('text_lines[{}] = {}'.format(i, text_lines[i]))
                    if ver_regexp.match(text_lines[i]):
                        logger.debug('matched regexp: {}'.format(ver_regexp_str))
                        logger.debug('line {}: {}'.format(i+1, text_lines[i]))
                        logger.info('found: {}'.format(text_lines[i]))
                        regexp_matched = 1
                        break
                if not regexp_matched:
                    logger.debug('std_out = {}'.format(str(std_out)))
                    logger.debug('std_err = {}'.format(str(std_err)))
                    tmp_dict = {'required': self.exit_req_ver, 'optional': self.exit_opt_ver}
                    if tmp_dict[entry_category]:
                        logger.error('error! could not match {} regexp: {}'.format(entry_category, ver_regexp_str))
                        return self.RET_CODE_ERROR
                    else:
                        logger.warning('warning! could not match {} regexp: {}'.format(entry_category, ver_regexp_str))
        return self.RET_CODE_SUCCESS




def main(cli_args_dict=None):
    """
    description:
        This is the main function of env_checker module
    input args:
        - cli_args_dict - dictionary contains arguments passed from cli
    return value:
        - none
    """
    ech = EnvChecker(cli_args_dict['xml_file_path'],
                     exit_required=cli_args_dict['exit_required'],
                     exit_req_ver=cli_args_dict['exit_req_ver'],
                     skip_required=cli_args_dict['skip_required'],
                     exit_optional=cli_args_dict['exit_optional'],
                     exit_opt_ver=cli_args_dict['exit_opt_ver'],
                     skip_optional=cli_args_dict['skip_optional'],
                     debug_level=cli_args_dict['debug_level'])
    ech.parse_xml_file_content()
    return ech.check_env()




# Call context
if __name__ == '__main__':
    cli_parser = argparse.ArgumentParser(prog=__prog__,
                                         description='check your environment before do anything')
    cli_parser.add_argument('-c',
                            '--config',
                            dest='xml_file_path',
                            metavar='XML_FILE_PATH',
                            default='env_checker.xml',
                            required=True,
                            help='configuration *.xml file path (default: %(default)s)',)
    cli_parser.add_argument('--xr',
                            dest='exit_required',
                            type=int,
                            choices=[0, 1],
                            default=1,
                            help='stop the script in case any required entry was not found (default: %(default)s)',)
    cli_parser.add_argument('--xo',
                            dest='exit_optional',
                            type=int,
                            choices=[0, 1],
                            default=0,
                            help='stop the script in case any optional entry was not found (default: %(default)s)',)
    cli_parser.add_argument('--exrv',
                            dest='exit_req_ver',
                            type=int,
                            choices=[0, 1],
                            default=1,
                            help='stop the script in case required entry was found with wrong version (default: %(default)s)',)
    cli_parser.add_argument('--exov',
                            dest='exit_opt_ver',
                            type=int,
                            choices=[0, 1],
                            default=0,
                            help='stop the script in case optional entry was found with wrong version (default: %(default)s)',)
    cli_parser.add_argument('--skip-required',
                            dest='skip_required',
                            action='store_true',
                            help="don't parse required entries in *.xml configuration file (default: %(default)s)",)
    cli_parser.add_argument('--skip-optional',
                            dest='skip_optional',
                            action='store_true',
                            help="don't parse optional entries in *.xml configuration file (default: %(default)s)",)
    cli_parser.add_argument('--debug',
                            dest='debug_level',
                            choices=['DEBUG', 'INFO', 'WARNING', 'NOTSET'],
                            default='INFO',
                            help='increasing debug level (default: %(default)s)')
    cli_parser.add_argument('--version',
                            action='version',
                            version='%(prog)s '+__version__)
    cli_args = cli_parser.parse_args(['-c', 'env_checker.xml',
                                      '--xr=1',
                                      '--xo=0',
                                      '--exrv=1',
                                      '--exov=0',
                                      #'--skip-required',
                                      #'--skip-optional',
                                      '--debug', 'DEBUG'])
    cli_args = cli_parser.parse_args()
    cli_args_dict = vars(cli_args)
#    print('cli_args_dict =', cli_args_dict)
    sys.exit(main(cli_args_dict=cli_args_dict))




#70D0:
# - colorized console output?
# - logger configuration in external file?
# - remove hard-coded test for cli args parsing
# - short method for showing param values is needed
# - more verbose console output for --xr, --xo, --exrv and --exov switches
# - min/max versions could be supported (new xml schema)
# - method parse_xml_file_content could be optimized

